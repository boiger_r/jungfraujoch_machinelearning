#python script for reading the data and doing the performance evaluation of all the methods  and computing the metrics 
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os
import csv
from datetime import datetime, timedelta
from sklearn.cluster import KMeans
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM

from sklearn import linear_model
from sklearn.neighbors import LocalOutlierFactor

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from operator import itemgetter
from itertools import groupby

from unsupervised_methods import *


def SDE_ground_truth(df1):
    #input df1: a pandas dataframe with the JUngfraujoch timeseries data of the year 2020  
    #output SDE_events_start: list of dates, when each dust event starts
    #output SDE_events_end: list of dates, when each dust event ends
    #df_SDE: a new dataframe, that includes only the dust event information
    
    SDE_events_start_round_up = ['2020-02-08 14:00:00',
                           '2020-02-28 17:00:00',
                           '2020-03-09 14:00:00', 
                           '2020-03-16 11:00:00',
                           '2020-03-19 16:00:00',
                           '2020-03-25 21:00:00',
                           '2020-04-06 08:00:00',
                           '2020-04-16 12:00:00',
                           '2020-04-21 06:00:00',
                           '2020-05-04 07:00:00',
                           '2020-05-08 13:00:00',
                           '2020-05-10 14:00:00',
                           '2020-05-13 00:00:00',
                           '2020-05-14 18:00:00',
                           '2020-05-16 18:00:00',
                           '2020-06-24 03:00:00',
                           '2020-07-09 23:00:00',
                           '2020-07-28 03:00:00',
                           '2020-08-01 00:00:00',
                           '2020-08-05 18:00:00',
                           '2020-08-09 13:00:00',
                           '2020-09-14 04:00:00',
                           '2020-09-17 16:00:00',
                           '2020-10-19 09:00:00',
                           '2020-11-06 09:00:00',
                           '2020-11-23 12:00:00',
                          ]
    SDE_events_start = pd.to_datetime(SDE_events_start_round_up)

    SDE_events_end_round_up = ['2020-02-09 06:00:00',
                           '2020-02-29 21:00:00',
                           '2020-03-11 07:00:00', 
                           '2020-03-18 15:00:00',
                           '2020-03-22 22:00:00',
                           '2020-03-31 18:00:00',
                           '2020-04-07 12:00:00',
                           '2020-04-17 23:00:00',
                           '2020-04-21 16:00:00',
                           '2020-05-05 19:00:00',
                           '2020-05-10 10:00:00',
                           '2020-05-11 06:00:00',
                           '2020-05-13 15:00:00',
                           '2020-05-16 05:00:00',
                           '2020-05-17 02:00:00',
                           '2020-06-29 00:00:00',
                           '2020-07-10 23:00:00',
                           '2020-07-29 03:00:00',
                           '2020-08-02 01:00:00',
                           '2020-08-08 12:00:00',
                           '2020-08-13 21:00:00',
                           '2020-09-15 20:00:00',
                           '2020-09-20 23:00:00',
                           '2020-10-23 09:00:00',
                           '2020-11-09 03:00:00',
                           '2020-11-25 03:00:00',
                          ]

    SDE_events_end = pd.to_datetime(SDE_events_end_round_up)
    df_SDE = df1['sde_event']
    return SDE_events_start, SDE_events_end, df_SDE


# +

def read_data_all(datafile):
    #input datafile: the path to the file, where the data are stored
    #output df1: pandas dataframe with all data inside, including the result of the currently used method
    # i.e. the column AE_SSA needs to be negative for more than 4 hours
    #output df_dates: pandas dataframe with the hours, dust events, dust events numbers
    #output SDE_events_start: list of dates, when each dust event starts
    #output SDE_events_end: list of dates, when each dust event ends
    #df_SDE: a new dataframe, that includes only the dust event information
    df = pd.read_hdf(datafile, 'df') 
    # restrict the data set to the time that start with the beginning of the first sde event and ends with end of the last sde event
    first_event_index = df['sde_event'].idxmax()
    last_event_index = df['sde_event'].where(df['sde_event']==1).last_valid_index()
    df_new = df.loc[first_event_index:last_event_index]
    # delete the first column with the DateTime
    df1 = df_new[df_new.columns[1:]]
    # find the elements, where AE_SSA is negative for more than 6 hours
    AE_neg = list()
    k2 = list()
    for i in range(len(df_new['AE_SSA'])):
        if df_new['AE_SSA'].iloc[i] < 0:
            k2.append(i)
            AE_neg.append(1)
        else:
            AE_neg.append(0)
    k1 = list()
    for el in k2:
        if el+5 in k2:
            if el+4 in k2:
                if el+3 in k2:
                    if el+2 in k2:
                        if el+1 in k2:
                            k1.append(el)
    
    # Add a column, where AE_SSA is negative for more than six hours (= 1), if this is not the case (=0)
    sde_event_AE = np.zeros_like(df_new['AE_SSA'].values)
    for i in k1:
        sde_event_AE[i:i+6] = np.array([1,1,1,1,1,1])

    df1['AE_neg'] = sde_event_AE    
    SDE_events_start, SDE_events_end, df_SDE = SDE_ground_truth(df1) 
    df_new = df1.copy()
    
    df_new['DateTimeUTC']=df_new.index
    df1 = df1.drop(['sde_event','sde_event_nr'], axis = 1)
    df_dates = df_new[['DateTimeUTC','sde_event','sde_event_nr']]
    

    return df1, df_dates, SDE_events_start, SDE_events_end, df_SDE
# -





def read_data(datafile):
    # like the function read_data_all, but giving back several dataset combinations
    #input datafile: the path to the file, where the data are stored
    #output datasets: list of 9 pandas dataframe with different combinations of features
    # i.e. the column AE_SSA needs to be negative for more than 4 hours
    #output df_dates: pandas dataframe with the hours, dust events, dust events numbers
    #output SDE_events_start: list of dates, when each dust event starts
    #output SDE_events_end: list of dates, when each dust event ends
    #df_SDE: a new dataframe, that includes only the dust event information

    
    
    
    df = pd.read_hdf(datafile, 'df') 
    # restrict the data set to the time that start with the beginning of the first sde event and ends with end of the last sde event
    first_event_index = df['sde_event'].idxmax()
    last_event_index = df['sde_event'].where(df['sde_event']==1).last_valid_index()
    df_new = df.loc[first_event_index:last_event_index]
    #df1 = df_new[df_new.columns[1:23]]
    # delete the first column with the DateTime
    df1 = df_new[df_new.columns[1:]]
    # find the elements, where AE_SSA is negative for more than 6 hours
    AE_neg = list()
    k2 = list()
    for i in range(len(df_new['AE_SSA'])):
        if df_new['AE_SSA'].iloc[i] < 0:
            k2.append(i)
            AE_neg.append(1)
        else:
            AE_neg.append(0)
    k1 = list()
    for el in k2:
        if el+5 in k2:
            if el+4 in k2:
                if el+3 in k2:
                    if el+2 in k2:
                        if el+1 in k2:
                            k1.append(el)
    
    # Add a column, where AE_SSA is negative for more than six hours (= 1), if this is not the case (=0)
    sde_event_AE = np.zeros_like(df_new['AE_SSA'].values)
    for i in k1:
        sde_event_AE[i:i+6] = np.array([1,1,1,1,1,1])

    df1['AE_neg'] = sde_event_AE    
    SDE_events_start, SDE_events_end, df_SDE = SDE_ground_truth(df1)   
    df1 = df1.drop(['sde_event','sde_event_nr'], axis = 1)
    
 
    newnames = [x for x in df1.columns if "D" in x]
    
    newnamesD = [x for x in newnames if "V" not in x]

    newnamesV = [x for x in newnames if "V" in x]

    df1_new = df1[df1.columns[0:22]]
    df1_new['tot_V'] = df1[df1.columns[173:-1]].sum(axis =1) / (324-173)
    df1_new['tot_S'] = df1[df1.columns[23:173]].sum(axis =1) / (173-23)
    df1_new['V_smaller500'] = df1[df1.columns[204:268]].sum(axis = 1) / (268-204)
    df1_new['V_bigger500'] = df1[df1.columns[268:-1]].sum(axis = 1) / (324-268)

    df1_new['S_smaller500'] = df1[df1.columns[53:117]].sum(axis = 1) / (117-53)
    df1_new['S_bigger500'] = df1[df1.columns[117:173]].sum(axis = 1) / (173-117)

    df1_new['Vratio500']= df1_new['V_bigger500']/df1_new['tot_V']
    df1_new['Sratio500']= df1_new['S_bigger500']/df1_new['tot_S']

    #datasets:
    # whole dataset
    dataset1 = df1 
    # only optical properties
    dataset2 = df1.copy()
    dataset2 = dataset2.drop(newnamesV , axis =1)
    dataset2 = dataset2.drop(newnamesD , axis =1)
    # only size distribution properties
    dataset3 = df1.copy()
    dataset3 = dataset3[newnamesD]
    # only volume distribution properties
    dataset4 = df1.copy()
    dataset4 = dataset4[newnamesV]
    # only non optical properties
    optical = dataset2.columns
    dataset5 = df1.copy()
    dataset5 = dataset5.drop(optical, axis =1)
    # optical + size distribution
    dataset6 = df1.copy()
    dataset6 = dataset6.drop(newnamesV,axis = 1)
    # optical + volume distribution
    dataset7 = df1.copy()
    dataset7 = dataset7.drop(newnamesD, axis =1 )
    # optical + Vtot, Stot
    dataset8 = df1.copy()
    dataset8 = dataset8[optical]
    dataset8['tot_V'] = df1[df1.columns[173:-1]].sum(axis =1) / (324-173)
    dataset8['tot_S'] = df1[df1.columns[23:173]].sum(axis =1) / (173-23)
    # optical + Vsmaller500, Vbigger500, Dsmaller500, Dbigger500
    dataset9 = df1.copy()
    dataset9 = dataset9[optical]
    dataset9['V_smaller500'] = df1[df1.columns[204:268]].sum(axis = 1) / (268-204)
    dataset9['V_bigger500'] = df1[df1.columns[268:-1]].sum(axis = 1) / (324-268)
    dataset9['S_smaller500'] = df1[df1.columns[53:117]].sum(axis = 1) / (117-53)
    dataset9['S_bigger500'] = df1[df1.columns[117:173]].sum(axis = 1) / (173-117)

    # optical + Vratio500, Dratio500
    dataset10 = df1.copy()
    dataset10 = dataset10[optical]
    dataset10['V_ratio500'] = dataset9['V_bigger500']/dataset8['tot_V']
    dataset10['S_ratio500'] = dataset9['S_bigger500']/dataset8['tot_S']


    datasets = [dataset1, dataset2, dataset3, dataset4, dataset5, dataset6, dataset7, dataset8, dataset9, dataset10]
    df_dates = df_new[['DateTimeUTC','sde_event','sde_event_nr']]


    return datasets, df_dates, SDE_events_start, SDE_events_end, df_SDE





def compute_dust_events(dataframe, multivariate):
    # input dataframe: dataframe of 0s and 1s
    # input multivariate: either True or False
    # output: dataframe with 0s and 1s, 1s are chosen if there where 4 1s in a row in the origianl dataframe
    if multivariate == True:
        dataframe1 = list()
        k2 = list()
        for i in range(len(dataframe['sde_event'])):
            if dataframe['sde_event'].iloc[i]>=0.5:
                k2.append(i)

        k1 = list()
        for el in k2:
            if el+3 in k2:
                if el+2 in k2:
                    if el+1 in k2:
                        k1.append(el)        
        sde_event_1s = np.zeros_like(dataframe['sde_event'].values)
        for j in k1:
            sde_event_1s[j:j+4]=np.array([1,1,1,1])

        dataframe['sde_event_predicted']=sde_event_1s
        return dataframe        

    else:  
        for col in dataframe.columns:
            dataframe1 = list()
            k2 = list()
            for i in range(len(dataframe[col])):
                if dataframe[col].iloc[i]>=0.5:
                    k2.append(i)

            k1 = list()
            for el in k2:
                if el+3 in k2:
                    if el+2 in k2:
                        if el+1 in k2:
                            k1.append(el)        
            sde_event_1s = np.zeros_like(dataframe[col].values)

            for j in k1:
                sde_event_1s[j:j+4]=np.array([1,1,1,1])

            dataframe[col+'predicted']=sde_event_1s

        return dataframe
def number_noevents_data(dataframe, noevent_col, event_col, multivariate):
    # count the number of sde /nosde events
    if multivariate == True:
        event_name = 'sde_event_predicted'
        num_nosde = 1
        num_sde = 1
        dataframe.loc[:,noevent_col]=0
        dataframe.loc[:,event_col]=0
        for k,v in dataframe.groupby((dataframe[event_name].shift() !=dataframe[event_name]).cumsum()):
            if v[event_name].all() ==0:
               # dataframe[noevent_col].loc[v.index]=num_nosde
                dataframe.loc[v.index, noevent_col]=num_nosde
                num_nosde = num_nosde+1
            if v[event_name].all() ==1:
               # dataframe[event_col].loc[v.index]=num_sde
                dataframe.loc[v.index, event_col]=num_sde
                num_sde = num_sde+1

    else:
        event_columns = [x for x in dataframe.columns if 'predicted' in x]
        #print(event_columns)
        for col_names in event_columns:
         #   print(col_names)
            num_nosde = 1
            num_sde = 1
            noevent_col = 'no_'+col_names+'_nr'
            event_col = col_names+'_nr'
            dataframe.loc[:,noevent_col]=0
            dataframe.loc[:,event_col]=0
            event_name = col_names
            for k,v in dataframe.groupby((dataframe[event_name].shift() !=dataframe[event_name]).cumsum()):
                if v[event_name].all() ==0:
                    dataframe.loc[v.index, noevent_col]=num_nosde
                    num_nosde = num_nosde+1
                if v[event_name].all() ==1:
                   # dataframe[event_col].loc[v.index]=num_sde
                    dataframe.loc[v.index, event_col]=num_sde
                    num_sde = num_sde+1
    return dataframe 

def number_noevents_dates(dataframe, event_name, event_col):
    # input dataframe: dataframe with 1s and 0s
    #input event_name: e.g. like "sde_event_nr"
    #input event_col: e.g. like "nosde_event_nr"
    #output dataframe: original dataframe that has additional columns
    #dataframe[event_col]=0
    # count the number, where there are no dust events in the dates dataframe
    dataframe.loc[:,event_col]=0
    num = 1
    for k,v in dataframe.groupby((dataframe[event_name].shift() !=dataframe[event_name]).cumsum()):
   #     print(f'[group{k}]')
   #     print(v[event_name])
   #     print('\n')
    
        if v[event_name].all() ==0:
            
            #dataframe[event_col].loc[v.index]=num
            dataframe.loc[v.index, event_col]=num
           # print(v.index)
            num = num+1
            
    return dataframe

def metric_sde(dates, qoi_val_pred, multivariate):
    #input dates: pandas dataframe, with dust events
    #input qoi_val_pred: dataframe with the predicted dust events
    #input multivariate: either True or False
    #result_df: performance measures of the method in a pandas dataframe
   
    # specify the rows of the output dataframe
    rowdict =  {
        0: '#sde_truth',
        1: '#nosde_truth',
        2: '#sde_pred',
        3: '#nosde_pred',   
        4:'TP' ,
        5: "FN",
        6: 'TN',
        7:'FP'} 
 
    if multivariate == True:
        result_df = pd.DataFrame()
        count_sde = 0
        count_nosde = 0
        # get the numbers of the true sde / nosde
        dust_event_nrs = dates['sde_event_nr'].unique()[1:]
        nodust_event_nrs = dates['nosde_event_nr'].unique()[1:]
        # for each true dust event: see whether the method also predicted a dust event, means at least 1 overlap
        for ev in (dust_event_nrs):
            #print('dust_event_number: ',ev)
            ev_index = dates[dates['sde_event_nr']==ev].index
            #print(qoi_val_pred['sde_event_predicted'].loc[ev_index].sum())
            if qoi_val_pred['sde_event_predicted'].loc[ev_index].sum() >0:
               # print(qoi_val_pred['sde_event_predicted'].loc[ev_index].sum())
               # if qoi_val_pred['sde_event_predicted'].loc[ev_index].sum() <4:
               #     print(ev_index)
                count_sde = count_sde+1
        # for each nosde:see whether the method also predicted not a dust event, means at least 1 overlap

        for noev in (nodust_event_nrs):
            #print('nodust_event_number: ',noev)
            noev_index = dates[dates['nosde_event_nr']==noev].index
            #print(qoi_val_pred['sde_event_predicted'].loc[noev_index].sum())
            if qoi_val_pred['sde_event_predicted'].loc[noev_index].sum() >0:
                count_nosde = count_nosde
            else:
                count_nosde = count_nosde +1
                
        # collect the numbers        
        N_sde_truth = len(dust_event_nrs) 
        N_nosde_truth = len(nodust_event_nrs)
        TP = count_sde+1
        FN = N_sde_truth - TP
        TN = count_nosde
        FP = N_nosde_truth - count_nosde
        N_sde_pred = qoi_val_pred['sde_event_predicted_nr'].max()
        N_nosde_pred = qoi_val_pred['nosde_event_predicted_nr'].max()

       # print('true number sde: ', N_sde_truth )
       # print('true number nosde: ', N_nosde_truth)
       # print('predicted number sde: ',N_sde_pred )

       # print('predicted number nosde: ', N_nosde_pred)
       # print('True positive: ', TP)
       # print('False negative: ',FN)
       # print('True negative: ', TN)
       # print('False positive: ',FP)
        # put the measures in a pandas dataframe
        result_df.loc[:,'all']= [N_sde_truth, N_nosde_truth, N_sde_pred, N_nosde_pred, TP, FN, TN, FP]
        
        result_df.rename(index = rowdict, inplace=True)
        
    else:
        result_df = pd.DataFrame()
        dust_event_nrs = dates['sde_event_nr'].unique()[1:]
        nodust_event_nrs = dates['nosde_event_nr'].unique()[1:]
        list1 = [x for x in qoi_val_pred.columns if 'predicted' in x and '_nr' not in x ]
        
        for col in list1:
            #print(col)
            count_sde = 0
            count_nosde = 0
            for ev in (dust_event_nrs):
                ev_index = dates[dates['sde_event_nr']==ev].index               
                if qoi_val_pred[col].loc[ev_index].sum() >0:
                    count_sde = count_sde+1

            for noev in (nodust_event_nrs):
                noev_index = dates[dates['nosde_event_nr']==noev].index
                if qoi_val_pred[col].loc[noev_index].sum() >0:
                    count_nosde = count_nosde
                else:
                    count_nosde = count_nosde +1
                    
            colname_short = col[9:-9]
            N_sde_truth = len(dust_event_nrs) 
            N_nosde_truth = len(nodust_event_nrs)
            TP = count_sde+1
            FN = N_sde_truth - TP
            TN = count_nosde
            FP = N_nosde_truth - count_nosde
            N_sde_pred = qoi_val_pred['sde_event'+colname_short+'predicted_nr'].max()
            N_nosde_pred = qoi_val_pred['no_sde_event'+colname_short+'predicted_nr'].max()

            result_df.loc[:,colname_short] = [N_sde_truth, N_nosde_truth, N_sde_pred, N_nosde_pred, TP, FN, TN, FP]
        result_df.rename(index = rowdict, inplace=True)
    return result_df

def acc_prec_rec(result):
    # input result: pandas dataframe with the counts of true and false positives, negatives
    #output result: add accuracy, precision, recall, specificity, F1 to the dataframe
    # accuracy = proportion of true results among the total number of cases examined        
    result.loc['accuracy',:] = (result.loc['TP']+result.loc['TN'])/(result.loc['TP']+result.loc['FP']+result.loc['FN']+result.loc['TN'])

    # precision = what proportion of predicted Positives is truly positive   
    result.loc['precision',:] = result.loc['TP']/(result.loc['TP']+result.loc['FP'])
    
    
    #recall = what proportion of actual Positives is correctly classified (= True positive rate)
    # = sensitivity
    result.loc['recall',:] = result.loc['TP']/(result.loc['TP']+result.loc['FN'])
    
    #specificity (=False positive rate)
    result.loc['specificity',:] = result.loc['TN']/(result.loc['TN'] + result.loc['FP'])


    # F1 score: number between 0 and 1, harmonic mean of precision and recall
    result.loc['F1',:] =2* result.loc['precision']*result.loc['recall']/(result.loc['precision']+result.loc['recall'])
   # confusion_matrix = [[TP, FP],[FN,TN]]
   # print(confusion_matrix)

   # print(np.array(confusion_matrix).sum(axis = 1))
   # if 
   # confusion_matrix = np.array(confusion_matrix)/np.array(confusion_matrix).sum(axis = 1)
    
    #print('accuracy: ', accuracy)
    #print('precision: ', precision)
    #print('sensitivity (recall): ', recall)
    #print('specificity: ', specificity)
    #print('F1: ',F1)
    return result #, confusion_matrix    




def classification_results(method, dataset,df_dates, multivariate ):
    #input method:  give the name of the method you want to use
    #input dataset: give the dataset in a pandas dataframe
    #input df_dates: the dataframe with time and dust events for comparison
    #input multivariate: specify either multivariate = True or fals
    #output result_acc: dataframe with number of sde /nosde, TP, FN, TN, FP, accuracy, precision, recall, specificty, F1
    #output dataframe_pred:dataframe with the predicted 0's and 1's for each feature and columns where the sde /nosde are counted
    
    # the specified classification methods (unsupervised_methods.py) are called and give back a dataframe with 0s and 1s
    if method == 'kmeans':
        dataframe = kmeans_class(dataset, multivariate)
    elif method == 'isolation_forest':
        dataframe = isolationforest_class(dataset, multivariate)
    elif method == 'OCSVM':
        dataframe = OCSVM_class(dataset, multivariate)
    elif method == 'OCSVM_SGD':
        dataframe = OCSVM_SGD_class(dataset, multivariate)
    elif method == 'LOF':
        dataframe = LocalOutlierFactor_class(dataset, multivariate)
        
    elif method == 'threshold':
        # please specify here the threshold that should be used
        dataframe = threshold_class(dataset, dataset.max()/5.0)
    elif method == 'interquartile':
        dataframe = interquartile_class(dataset)
    

    dataframe_pred = compute_dust_events(dataframe, multivariate)
    df_dates_new = number_noevents_dates(df_dates, 'sde_event_nr', 'nosde_event_nr')
    dataframe_pred = number_noevents_data(dataframe_pred, 'nosde_event_predicted_nr', 'sde_event_predicted_nr', multivariate)

    
    result = metric_sde(df_dates_new, dataframe_pred, multivariate)

    result_acc = acc_prec_rec(result)
            
    return result_acc, dataframe_pred



                


