# +
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os
import csv
from datetime import datetime, timedelta
from numpy.random import seed


import tensorflow as tf
tf.keras.backend.set_floatx('float64')
from tensorflow.keras import regularizers
from operator import itemgetter
from itertools import groupby
import json
# load csv module
import csv
# -

seed1 = 300
seed(seed1)

tf.random.set_seed(seed1)

from tensorflow import keras
import keras_tuner as kt


def model_builder(hp):
    # build the surrogate model
    
    model = keras.Sequential()
    
    model.add(keras.Input(shape=(24)))
       
    hp_depth = hp.Int('depth',min_value = 1, max_value = 10, step = 1)
    hp_units = hp.Int('units',min_value = 2, max_value = 20, step = 1)
    
    hp_learning_rate = hp.Choice('learning_rate', values = [1e-4, 1e-5,1e-3])
    

    for i in range(hp_depth):
        model.add(keras.layers.Dense(units = hp_units, activation = 'relu') ) #,kernel_regularizer=tf.keras.regularizers.L1(0.01),activity_regularizer=tf.keras.regularizers.L2(0.01)))
    
    
    model.add(keras.layers.Dense(1, activation ='sigmoid'))

     
    optimizer = tf.keras.optimizers.Adam(learning_rate=hp_learning_rate)
    #optimizer = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.9)
    model.compile(
        loss='binary_crossentropy',
        optimizer=optimizer,#'sgd',
        metrics=['accuracy']
    )


    print(model.metrics)

    return model


def build_NN(dimX, dimY, config):
    # build the surrogate model
    


    #metrics = [tf.keras.losses.mse, tf.keras.losses.mean_absolute_percentage_error, tf.keras.losses.mean_absolute_error]
    
    input_layer = tf.keras.layers.Dense(input_shape=(dimX,), units=dimX)
    gaussian_noise_layer = tf.keras.layers.GaussianNoise(0.01)

    hidden_layers = []

    for i in range(config['depth']):
        layer = tf.keras.layers.Dense(units = config['width'], activation =config['activation'],kernel_regularizer=tf.keras.regularizers.L1(0.001)) #activity_regularizer=tf.keras.regularizers.L2(0.01))
        hidden_layers.append(layer)
    
    output_layer = tf.keras.layers.Dense(units = 1, activation = 'sigmoid' )

    layers = [input_layer]  + [gaussian_noise_layer] + hidden_layers + [output_layer]

    model = tf.keras.models.Sequential(layers)
    
    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-2)
    #optimizer = tf.keras.optimizers.SGD(learning_rate=0.01, momentum=0.9)
    model.compile(
        loss='binary_crossentropy',
        optimizer=optimizer,
        metrics=['accuracy']
    )


    print(model.metrics)

    return model


scale_min = 0
scale_max = 1
def scaling(df,df_min,df_max,scale_min,scale_max):
    df_std = (df-df_min) / (df_max-df_min)
    df_scaled = df_std* (scale_max - scale_min)+scale_min
    return df_scaled

def scaling_reverse(df,df_min,df_max,scale_min,scale_max):
    df_scaled_reverse = (df-scale_min)/(scale_max-scale_min)*(df_max-df_min)+df_min
    return df_scaled_reverse


import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--number',
                        help='Number of datafile.')
args = parser.parse_args()
index = args.number




datafile = '/data/project/general/aerosolretriev/Jungfraujoch_data/NeuralNetwork/models_29_11_2022/train'+str(index)+'.h5'

modelpath = '/data/project/general/aerosolretriev/Jungfraujoch_data/NeuralNetwork/models_07_12_2022/model7/model_'+str(index)
dvar_train = pd.read_hdf(datafile,key = 'dvar')
qoi_train= pd.read_hdf(datafile,key = 'qoi')


qoi_min = np.min(qoi_train, axis = 0)
qoi_max = np.max(qoi_train, axis = 0)
dvar_min = np.min(dvar_train, axis = 0)
dvar_max = np.max(dvar_train, axis = 0)

dvar_train_scaled = scaling(dvar_train, dvar_min, dvar_max, scale_min, scale_max)

# Hyperparameter scan
tuner1 = kt.Hyperband(model_builder, 
                    objective = 'val_accuracy', 
                    max_epochs = 30, 
                    factor =3, 
                     directory = 'my_dir_new',
                     project_name = 'sars_epitone_region_classifier',
                     seed=seed1, overwrite=True
                    )
# -

stop_early = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=5)

# +
#tuner1.search(np.asarray(dvar_train.values).astype('float32'), np.asarray(qoi_train.values).astype('float32'), epochs=50, validation_split=0.2, callbacks=[stop_early])


# +
#best_hps=tuner1.get_best_hyperparameters(num_trials=1)[0]
# -


# train final model

# config = {'depth': best_hps.get('depth'), 
#          'width': best_hps.get('units'),
#          'activation': 'relu',
#          'learning_rate': best_hps.get('learning_rate'),
#          'batch_size': 32,
#           'epoch': 300}


config = {'depth': 2, 
          'width': 8,
          'activation': 'relu',
          'learning_rate': 0.001,
          'batch_size': 32,
          'epoch': 300}
print(config)

dimX = dvar_train.shape[1]
dimY = 1
surr = build_NN(dimX,dimY,config)


history = surr.fit(np.asarray(dvar_train_scaled.values).astype('float32'),
                     np.asarray(qoi_train.values).astype('float32'),
                     batch_size=config['batch_size'],
                     epochs=config['epoch'],
                     validation_split=0.6
                   
                  )


surr.save(modelpath)
np.save(modelpath+'/my_history.npy',history.history)
#history=np.load('my_history.npy',allow_pickle='TRUE').item()

# +


# open file for writing, "w" is writing
w = csv.writer(open(modelpath+'/my_history.csv', "w"))

# loop over dictionary keys and values
for key, val in history.history.items():

    # write every key and value to file
    w.writerow([key, val])



# +
# open file for writing, "w" is writing
w = csv.writer(open(modelpath+'/config.csv', "w"))

# loop over dictionary keys and values
for key, val in config.items():

    # write every key and value to file
    w.writerow([key, val])


