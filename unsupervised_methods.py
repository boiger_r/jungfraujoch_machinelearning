# python script, that includes all the unsupervised methods
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import os
import csv
from datetime import datetime, timedelta
from sklearn.cluster import KMeans
from sklearn.ensemble import IsolationForest
from sklearn.svm import OneClassSVM

from sklearn import linear_model
from sklearn.neighbors import LocalOutlierFactor

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from operator import itemgetter
from itertools import groupby


#

def threshold_class(df, threshold):
    #Input df: pandas dataframe of features
    #Input threshold: specifies the threshold that is taken
    #output dataframe: gives back a dataframe with 0s and 1s
    #For each feature and every hour the function checks, whether the value is above the threhsold, then we define it as 1, or below the threshold, then define it as 0.
    dataframe = df.copy()   
    dataframe[dataframe < threshold] = 0
    dataframe[dataframe > 0] = 1  
    dataframe = dataframe.add_prefix('sde_event')
    return dataframe

def interquartile_class(df):
    #Input df: pandas dataframe of features
    #output dataframe: gives back a dataframe with 0s and 1s
    # method explained, e.g. https://www.thoughtco.com/what-is-the-interquartile-range-rule-3126244
    # similar to code:https://towardsdatascience.com/anomaly-detection-in-time-series-sensor-data-86fd52e62538
    
    dataframe = df.copy()
    anomaly_dataframe = pd.DataFrame()
    # calculate the interquartile range which is the difference between the 75th (Q3) and 25th(Q1) percentiles
    for col in dataframe.columns:
        q1_pc1, q3_pc1 = dataframe[col].quantile([0.25, 0.75])
        iqr_pc1 = q3_pc1 - q1_pc1

    # Calculate upper and lower bounds for outlier

        lower_pc1 = q1_pc1 - (1.5*iqr_pc1)
        upper_pc1 = q3_pc1 + (1.5*iqr_pc1)

    # Filter the data points that fall outside the upper and lower bounds and flag them as outliesrs, respectively 1 (sde)
        anomaly_dataframe[col] = ((dataframe[col]> upper_pc1)| (dataframe[col]< lower_pc1)).astype('int')
    anomaly_dataframe = anomaly_dataframe.add_prefix('sde_event')
    return anomaly_dataframe

def getDistanceByPoint(data, model):
    # function, that calculates the distance between a point and centroid of a cluster and returns the distances in pandas series
    distance = []
    for i in range(0,len(data)):
        Xa = np.array(data.iloc[i])
        Xb = model.cluster_centers_[model.labels_[i]-1]
        distance.append(np.linalg.norm(Xa-Xb))
    return pd.Series(distance, index = data.index)

def kmeans_class(df, multivariate = True):
    #Input df: pandas dataframe of features
    #Input multivariate: either True or False
    #output dataframe: gives back a dataframe with 0s and 1s
    # code taken from: https://towardsdatascience.com/anomaly-detection-in-time-series-sensor-data-86fd52e62538
    
    dataframe = df.copy()
    anomaly_dataframe = pd.DataFrame()
    # the outliers fraction comes from the original dataset
    outliers_fraction = 0.17
    
    
    if multivariate == False:
        for col in dataframe.columns:
            # choose two clusters and a random state
            kmeans = KMeans(n_clusters=2, random_state=42)
            # fit the data
            kmeans.fit(dataframe[col].values.reshape(-1, 1))
            labels = kmeans.predict(dataframe[col].values.reshape(-1, 1))
            unique_elements, counts_elements = np.unique(labels, return_counts = True)
            clusters = np.asarray((unique_elements, counts_elements))
            # get the distance between each point and its nearest centroid. The biggest distances are considered as anomaly
            distance = getDistanceByPoint(dataframe[col], kmeans)
            # number of observations that equate to the 17% of the entire data set
            number_of_outliers = int(outliers_fraction*len(distance))
            # Take the minimum of the largest 17% of the distances as the threshold
            threshold = distance.nlargest(number_of_outliers).min()
            anomaly_dataframe['sde_event'+col] = (distance >=threshold).astype(int)
    else:
        kmeans = KMeans(n_clusters=2, random_state=42)
        kmeans.fit(dataframe.values)
        labels = kmeans.predict(dataframe.values)
        unique_elements, counts_elements = np.unique(labels, return_counts = True)
        clusters = np.asarray((unique_elements, counts_elements))
        distance = getDistanceByPoint(dataframe, kmeans)
        number_of_outliers = int(outliers_fraction*len(distance))
        threshold = distance.nlargest(number_of_outliers).min()
        anomaly_dataframe['sde_event'] = (distance >=threshold).astype(int)
        
        
        
    return anomaly_dataframe

def isolationforest_class(df, multivariate = True):
    #Input df: pandas dataframe of features
    #Input multivariate: either True or False
    #output dataframe: gives back a dataframe with 0s and 1s
    #https://towardsdatascience.com/anomaly-detection-in-time-series-sensor-data-86fd52e62538
    dataframe = df.copy()
    anomaly_dataframe = pd.DataFrame()
    # Assume that 17% of the entire data set are anomalies
    outliers_fraction = 0.17

    if multivariate == True:
        model = IsolationForest(contamination = outliers_fraction)
        model.fit(dataframe.values)
        result = model.predict(dataframe.values)
        dataframe['result'] = result
        anomaly_dataframe['sde_event'] = dataframe['result']
        
    else:
        for col in dataframe.columns:
            model = IsolationForest(contamination = outliers_fraction)
            model.fit(dataframe[col].values.reshape(-1, 1))
            result = model.predict(dataframe[col].values.reshape(-1, 1))
            dataframe['result'] = result
            anomaly_dataframe['sde_event'+col] = dataframe['result']
    return anomaly_dataframe

def OCSVM_class(df, multivariate = True):
    #Input df: pandas dataframe of features
    #Input multivariate: either True or False
    #output dataframe: gives back a dataframe with 0s and 1s
    # e.g. https://towardsdatascience.com/support-vector-machine-svm-for-anomaly-detection-73a8d676c331
    
    dataframe = df.copy()
    anomaly_dataframe = pd.DataFrame()
    
    if multivariate == True:
        OCSVM_all = OneClassSVM(kernel='rbf', gamma='scale', tol=0.001, nu=0.6, shrinking=True, cache_size=200, verbose=False, max_iter=- 1)
 
        #        OCSVM_all = OneClassSVM(kernel='sigmoid', degree=3, gamma='auto', coef0=0.2, tol=0.001, nu=0.5, shrinking=True, cache_size=200, verbose=False, max_iter=- 1)
        OCSVM_all.fit(dataframe.values)

        OCSVM_pred = OCSVM_all.predict(dataframe.values)

        OCSVM_pred = OCSVM_pred*(-1)
        OCSVM_pred[OCSVM_pred ==-1]=0
        dataframe['result'] = OCSVM_pred
        anomaly_dataframe['sde_event']=dataframe['result']
    else:
        for col in dataframe.columns:
            OCSVM_all = OneClassSVM(kernel='rbf', gamma='scale', tol=0.001, nu=0.6, shrinking=True, cache_size=200, verbose=False, max_iter=- 1)
 
            #            OCSVM_all = OneClassSVM(kernel='sigmoid', degree=3, gamma='auto', coef0=0.2, tol=0.001, nu=0.5, shrinking=True, cache_size=200, verbose=False, max_iter=- 1)
            OCSVM_all.fit(dataframe[col].values.reshape(-1, 1))

            OCSVM_pred = OCSVM_all.predict(dataframe[col].values.reshape(-1, 1))

            OCSVM_pred = OCSVM_pred*(-1)
            OCSVM_pred[OCSVM_pred ==-1]=0
            dataframe['result'] = OCSVM_pred
            anomaly_dataframe['sde_event'+col] = dataframe['result']
    return anomaly_dataframe    

def OCSVM_SGD_class(df, multivariate = True):
    #Input df: pandas dataframe of features
    #Input multivariate: either True or False
    #output dataframe: gives back a dataframe with 0s and 1s
    dataframe = df.copy()
    anomaly_dataframe = pd.DataFrame()
    
    if multivariate == True:
        OCSVM_all = linear_model.SGDOneClassSVM(nu = 0.6,random_state = 42, shuffle = False, learning_rate = 'constant',eta0=0.1, average = True)

        
        #OCSVM_all = linear_model.SGDOneClassSVM(nu = 0.1,random_state = 42, shuffle = True, learning_rate = 'optimal',eta0=0.0, average = False)
        OCSVM_all.fit(dataframe.values)

        OCSVM_pred = OCSVM_all.predict(dataframe.values)

        OCSVM_pred = OCSVM_pred*(-1)
        OCSVM_pred[OCSVM_pred ==-1]=0
        dataframe['result'] = OCSVM_pred
        anomaly_dataframe['sde_event']=dataframe['result']
        
    else:
        for col in dataframe.columns:
            OCSVM_all = linear_model.SGDOneClassSVM(nu = 0.6,random_state = 42, shuffle = False, learning_rate = 'constant',eta0=0.1, average = True)


            #OCSVM_all = linear_model.SGDOneClassSVM(nu = 0.1,random_state = 42, shuffle = True, learning_rate = 'optimal',eta0=0.0, average = False)
            OCSVM_all.fit(dataframe[col].values.reshape(-1, 1))

            OCSVM_pred = OCSVM_all.predict(dataframe[col].values.reshape(-1, 1))

            OCSVM_pred = OCSVM_pred*(-1)
            OCSVM_pred[OCSVM_pred ==-1]=0
            dataframe['result'] = OCSVM_pred
            anomaly_dataframe['sde_event'+col] = dataframe['result']     
            
    return anomaly_dataframe

def LocalOutlierFactor_class(df1, multivariate = True):
    #Input df: pandas dataframe of features
    #Input multivariate: either True or False
    #output dataframe: gives back a dataframe with 0s and 1s
    #e.g. https://towardsdatascience.com/anomaly-detection-with-local-outlier-factor-lof-d91e41df10f2
    
    dataframe = df1.copy()
    anomaly_dataframe = pd.DataFrame()
    
    if multivariate == True:
        LOF_all = LocalOutlierFactor(n_neighbors=50, metric='minkowski',p= 1, contamination = 0.17, n_jobs = -1, novelty = False )
        #LOF_all = LocalOutlierFactor(n_neighbors=20, metric='minkowski',p= 2, contamination = 0.17, n_jobs = -1, novelty = False )

        LOF_pred = LOF_all.fit_predict(dataframe.values)
    
        LOF_pred = LOF_pred*(-1)
        LOF_pred[LOF_pred==-1]=0
        dataframe['result']=LOF_pred
        anomaly_dataframe['sde_event']=dataframe['result']
    else:
        for col in dataframe.columns:
            #LOF_all = LocalOutlierFactor(n_neighbors=50, metric='minkowski',p= 1, contamination = 0.17, n_jobs = -1, novelty = False )
            
            LOF_all = LocalOutlierFactor(n_neighbors=20, metric='minkowski',p= 2, contamination = 0.17, n_jobs = -1 )

            LOF_pred = LOF_all.fit_predict(dataframe[col].values.reshape(-1, 1))
    
            LOF_pred = LOF_pred*(-1)
            LOF_pred[LOF_pred==-1]=0
            dataframe['result']=LOF_pred
            anomaly_dataframe['sde_event'+col]=dataframe['result']
    return anomaly_dataframe

